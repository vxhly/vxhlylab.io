---
author: 星火燎原@vxhly
title: jQuery 各个版本的下载链接
categories: [web]
tags: [JavaScript, jQuery]
date: 2016-08-18 17:13:24
---

::: tip
提供 jQuery 所有版本的下载链接和 CDN 链接
:::
<!-- more -->

> 部分下载链接来至 \<//code.jquery.com/>

# jquery-3.2.1（最新）

``` html
<script src="////code.jquery.com/jquery-3.2.1.min.js"></script>
```

# jquery-3.2.0

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-3.2.0.min.js"></script>
```

## 下载链接

> [jquery-3.2.1.zip](http://oss-blog.test.upcdn.net/jquery-3.2.1.zip?attname=)

# jquery-3.0.0

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-3.0.0.min.js"></script>
```

## 下载链接

> [jquery-3.0.0.zip](http://oss-blog.test.upcdn.net/jquery-3.0.0.zip?attname=)

# jquery-2.2.4

> jquery-2.2.4 (推荐目前最稳定的, 不会出现延时打不开情况)

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-2.2.4.min.js"></script>
```

# jquery-2.1.4

> jquery-2.1.4 (注！ jquery-2.0 以上版本不再支持 IE 6/7/8)

## 百度压缩版引用地址

``` html
<script src="//libs.baidu.com/jquery/2.1.4/jquery.min.js"></script>
```

## 微软压缩版引用地址

``` html
<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-2.1.4.min.js"></script>
```

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
```

## 下载链接

> [jquery-2.1.4.zip](http://oss-blog.test.upcdn.net/jquery-2.1.4.zip?attname=)

# jquery-2.1.1

## 百度压缩版引用地址

``` html
<script src="//libs.baidu.com/jquery/2.1.1/jquery.min.js"></script>
```

## 微软压缩版引用地址

``` html
<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-2.1.1.min.js"></script>
```

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
```

## 下载链接

> [jquery-2.1.1.zip](http://oss-blog.test.upcdn.net/jquery-2.1.1.zip?attname=)

# jquery-2.0.0

## 百度压缩版引用地址

``` html
<script src="//libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
```

## 微软压缩版引用地址

``` html
<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-2.0.0.min.js"></script>
```

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-2.0.0.min.js"></script>
```

## 下载链接

> [jquery-2.0.3.zip](http://oss-blog.test.upcdn.net/jquery-2.0.3.zip?attname=)<br>
> [jquery-2.0.0.zip](http://oss-blog.test.upcdn.net/jquery-2.0.0.zip?attname=)

# jquery-1.11.3

## 百度压缩版引用地址

``` html
<script src="//libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
```

## 微软压缩版引用地址

``` html
<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.11.3.min.js"></script>
```

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
```

## 下载链接

> [jquery-1.11.3.zip](http://oss-blog.test.upcdn.net/jquery-1.11.3.zip?attname=)

# jquery-1.11.1

## 百度压缩版引用地址

``` html
<script src="//libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
```

## 微软压缩版引用地址

``` html
<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.11.1.min.js"></script>
```

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
```

## 下载链接

> [jquery-1.11.1.zip](http://oss-blog.test.upcdn.net/jquery-1.11.1.zip?attname=)

# jquery-1.10.2

## 百度压缩版引用地址

``` html
<script src="//libs.baidu.com/jquery/1.10.2/jquery.min.js"></script>
```

## 微软压缩版引用地址

``` html
<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.10.2.min.js"></script>
```

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
```

## 下载链接

> [jquery-1.10.2.zip](http://oss-blog.test.upcdn.net/jquery-1.10.2.zip?attname=)

# jquery-1.9.1

## 百度压缩版引用地址

``` html
<script src="//libs.baidu.com/jquery/1.9.1/jquery.min.js"></script>
```

## 微软压缩版引用地址

``` html
<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.1.min.js"></script>
```

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
```

## 下载链接

> [jquery-1.9.1.zip](http://oss-blog.test.upcdn.net/jquery-1.9.1.zip?attname=)

# jquery-1.8.3

## 百度压缩版引用地址

``` html
<script src="//libs.baidu.com/jquery/1.8.3/jquery.min.js"></script>
```

## 微软压缩版引用地址

``` html
<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.8.3.min.js"></script>
```

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-1.8.3.min.js"></script>
```

## 下载链接

> [jquery-1.8.3.zip](http://oss-blog.test.upcdn.net/jquery-1.8.3.zip?attname=)

# jquery-1.7.2

## 百度压缩版引用地址

``` html
<script src="//libs.baidu.com/jquery/1.7.2/jquery.min.js"></script>
```

## 微软压缩版引用地址

``` html
<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.7.2.min.js"></script>
```

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-1.7.2.min.js"></script>
```

## 下载链接

> [jquery-1.7.2.zip](http://oss-blog.test.upcdn.net/jquery-1.7.2.zip?attname=)

# jquery-1.6.4

## 百度压缩版引用地址

``` html
<script src="//libs.baidu.com/jquery/1.6.4/jquery.min.js"></script>
```

## 微软压缩版引用地址

``` html
<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.6.4.min.js"></script>
```

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-1.6.4.min.js"></script>
```

## 下载链接

> [jquery-1.6.4.zip](http://oss-blog.test.upcdn.net/jquery-1.6.4.zip?attname=)

# jquery-1.5.2

## 百度压缩版引用地址

``` html
<script src="//libs.baidu.com/jquery/1.5.2/jquery.min.js"></script>
```

## 微软压缩版引用地址

``` html
<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.5.2.min.js"></script>
```

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-1.5.2.min.js"></script>
```

## 下载链接

> [jquery-1.5.2.zip](http://oss-blog.test.upcdn.net/jquery-1.5.2.zip?attname=)

# jquery-1.4.4

## 百度压缩版引用地址

``` html
<script src="//libs.baidu.com/jquery/1.4.4/jquery.min.js"></script>
```

## 微软压缩版引用地址

``` html
<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.4.4.min.js"></script>
```

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-1.4.4.min.js"></script>
```

## 下载链接

> [jquery-1.4.4.zip](http://oss-blog.test.upcdn.net/jquery-1.4.4.zip?attname=)

# jquery-1.4.2

## 百度压缩版引用地址

``` html
<script src="//libs.baidu.com/jquery/1.4.2/jquery.min.js"></script>
```

## 微软压缩版引用地址

``` html
<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.4.2.min.js"></script>
```

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-1.4.2.min.js"></script>
```

## 下载链接

> [jquery-1.4.2.zip](http://oss-blog.test.upcdn.net/jquery-1.4.2.zip?attname=)

# jquery-1.3.2

## 百度压缩版引用地址

``` html
<script src="//libs.baidu.com/jquery/1.3.2/jquery.min.js"></script>
```

## 微软压缩版引用地址

``` html
<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.3.2.min.js"></script>
```

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-1.3.2.min.js"></script>
```

## 下载链接

> [jquery-1.3.2.zip](http://oss-blog.test.upcdn.net/jquery-1.3.2.zip?attname=)

# jquery-1.2.3

## 百度压缩版引用地址

``` html
<script src="//libs.baidu.com/jquery/1.2.3/jquery.min.js"></script>
```

## 微软压缩版引用地址

``` html
<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.2.3.min.js"></script>
```

## 官网 jquery 压缩版引用地址

``` html
<script src="//code.jquery.com/jquery-1.2.3.min.js"></script>
```

## 下载链接

> [jquery-1.2.3.zip](http://oss-blog.test.upcdn.net/jquery-1.2.3.zip?attname=)

