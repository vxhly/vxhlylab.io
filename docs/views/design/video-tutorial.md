---
author: 星火燎原@vxhly
title: 【发福利】视频教程
categories: [design]
tags: [Node.js, HTML5, Security, MySQL, Liunx]
date: 2017-12-10 17:09:06
---

::: tip
最近得一套视频教程, 里面包含 Node.js、Angular.js、WEB 开发、MySQL 等视频教程, 全是百度云链接, 尽快保存以防被和谐
:::
<!-- more -->

# Angular

> 链接：<https://pan.baidu.com/s/1jId3Ov8> 密码：v24x

# MySQL

> 链接：<https://pan.baidu.com/s/1c1SkGTy> 密码：cqfm

# Node.js

> 链接：<https://pan.baidu.com/s/1pLMERwv> 密码：bczq 链接：<https://pan.baidu.com/s/1dFvUZKH> 密码： pv9k

# PHP(包含 ThinkPHP 和 Laravel)

> 链接：<https://pan.baidu.com/s/1qYlzeES> 密码：94sh 链接：<https://pan.baidu.com/s/1o7UANKA> 密码：nnsl 链接：<https://pan.baidu.com/s/1dFwDy5z> 密码：8yuu

# Python

> 链接：<https://pan.baidu.com/s/1dE20p5v> 密码：guxn

# React.js

> 链接：<https://pan.baidu.com/s/1sk93oXv> 密码：26xn

# Vue.js

> 链接：<https://pan.baidu.com/s/1sl9d6CP> 密码：255c

# 安全(开发中需要注意的安全)

> 链接：<https://pan.baidu.com/s/1migh5Tu> 密码：xdq8

# 大数据

> 链接：<https://pan.baidu.com/s/1dFCPP8d> 密码：74m7

# 服务器(Nginx)

> 链接：<https://pan.baidu.com/s/1o8OIKZK> 密码：1a3v

# 前端

> 链接：<https://pan.baidu.com/s/1jIR8IUy> 密码：81g4

# 算法

> 链接：<https://pan.baidu.com/s/1kVeNBE3> 密码：923c

# 微信小程序

> 链接：<https://pan.baidu.com/s/1c2rTl8s> 密码：v9mq

# Android

> <http://pan.baidu.com/s/1kUGIVUf>

# ES6

> 链接：<https://pan.baidu.com/s/1c1FwfYS> 密码：ygog

# MongoDB

> 链接：<https://pan.baidu.com/s/1kVgSlCF> 密码：vvnn

