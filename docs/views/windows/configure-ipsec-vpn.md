---
author: 星火燎原@vxhly
title: IPSec VPN 的配置
categories: [windows]
tags: [Security, Service]
date: 2017-03-02 10:02:33
---

::: tip
IPSec VPN 指采用 IPSec 协议来实现远程接入的一种 VPN 技术, 本篇文章介绍在虚拟机下搭建 IPSec VPN 的全过程（使用 L2TP 连接方式） 
:::
<!-- more -->

# 环境需求

> 以下实验机全为虚拟机环境。

注意： 外网 IP 地址段： `20.1.1.0` 内网 IP 地址段： `10.1.1.0` VPN 地址池： `10.1.1.100~10.1.1.200` 

## 虚拟环境

### VPN 客户机

**要求：**

1. Windows 系统为： `Windows XP` 
2. 网络适配器连至 `VMnet2` , IP 地址配置为： `20.1.1.2` 

**作用：**

1. 作为 VPN 客户端连接 VPN 服务器提供的 VPN 服务
2. 作为 CA 客户端, 向 CA 服务器申请 CA 客户端证书

### VPN 服务器

**要求：**

1. Windows 系统为： `Windows 2003` 
2. 网络适配器连至 `VMnet2` , IP 地址配置为： `20.1.1.1` 
3. 网络适配器连至 `VMnet1` , IP 地址配置为： `10.1.1.1` 

**作用：**

1. 作为 VPN 服务器为 VPN 客户端提供 VPN 服务
2. 作为 VPN 服务器向 CA 服务器申请 IPSec 证书

### CA 服务器

**要求：**

1. Windows 系统为： `Windows 2003` 
2. 网络适配器连至 `VMnet1` , IP 地址配置为： `10.1.1.2` 

**作用：**

1. 作为 CA 服务器搭建 CA 服务
2. 作为 CA 服务器向 VPN 客户端和 VPN 服务端颁发相关证书

## 网络连通性

> 需保证 VPN 服务器的 VMnet1 可以 ping 通 CA 服务器的 VMnet1

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-1.png)

> 需保证 VPN 客户端的 VMnet2 可以 ping 通 VPN 服务器的 VMnet2

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-2.png)

> 需保证 VPN 客户端的 VMnet2 不可以 ping 通 CA 服务器的 VMnet1

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-3.png)

# VPN 服务器搭建 VPN 服务

## 创建 VPN 用户

创建 VPN 用户： user, 设置其远程访问权限（拨入或 VPN）为通过远程访问策略控制访问<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-4.png)

创建组： vpn, 将用户 user 加入 vpn 组<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-5.png)

## 开启 VPN 服务

### 开启路由和远程访问

开启路由和远程访问<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-6.png)

启用远程访问（拨号或 VPN）<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-7.png)

选择远程访问方式为 VPN<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-8.png)

### 选择连接外网接口

选择连接外网接口<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-9.png)

### 配置 VPN 地址池

配置 VPN 地址池<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-10.png)

VPN 地址池配置为： `10.1.1.100~10.1.1.200` <br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-111.png)

### 配置身份验证方式并完成配置

配置身份验证方式<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-12.png)

完成配置<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-13.png)

### 新建远程访问策略

新建远程访问策略<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-14.png)

自定义策略<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-15.png)

### 添加策略并保存

允许特殊时间段内才能访问<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-16.png)

允许规定用户组才能访问<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-17.png)

授予远程访问权限<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-18.png)

完成向导<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-19.png)

### 网络连通性

> 此时 VPN 客户端的 VMnet2 不可以 ping 通 VPN 服务器的 VMnet2 和 CA 服务器的 VMnet1

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-20.png)

# VPN 客户端连接 VPN 服务器

## 新建一个连接

新建一个连接<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-21.png)

设置连接类型<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-22.png)

使用 VPN 连接<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-23.png)

## 配置连接的 IP 地址

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-24.png)

## 使用 VPN 账户连接 VPN 服务器

连接 VPN 服务器<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-25.png)

此时的 VPN 连接状态为 PPTP<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-26.png)

此时的 IP 地址配置情况<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-27.png)

## 网络连通性

> 此时 VPN 客户端的 VMnet2 可以 ping 通 VPN 服务器的 VMnet1 和 CA 服务器的 VMnet1

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-28.png)

> 此时 VPN 客户端的 VMnet2 不可以 ping 通 VPN 服务器的 VMnet2

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-29.png)

# CA 服务器搭建 CA 服务

## 开启 IIS 服务

开启 IIS 服务<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-30.png)

开启 ASP 配置<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-31.png)

完成配置<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-32.png)

## 安装 CA 证书服务

安装 CA 证书服务<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-33.png)

CA 类型为独立根 CA<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-34.png)

完成安装, 本地访问测试<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-35.png)

# VPN 服务器向 CA 服务器申请 IPSec 证书

申请 IPSec 证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-36.png)

提交一个高级证书申请<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-37.png)

申请 IPSec 证书, 并将证书保存在本地计算机储存中<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-38.png)

申请成功<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-39.png)

## CA 服务器颁布 IPSec 证书

颁布 IPSec 证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-40.png)

## VPN 服务器安装导入 IPSec 证书

### 查看挂起的证书并安装

查看挂起的证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-41.png)

选择 IPSec 证书并安装<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-42.png)

安装成功<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-43.png)

### 下载证书并导入

下载证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-44.png)

下载 IPSec 证书和证书链<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-45.png)

将证书和证书链导入受信任的证书颁发机构(计算机)中<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-46.png)

证书导入成功显示<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-47.png)

证书链导入成功显示<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-48.png)

# VPN 客户机向 CA 服务器申请客户端证书

申请客户端证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-49.png)

提交一个高级证书申请<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-50.png)

申请 IPSec 证书, 并将证书保存在本地计算机储存中<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-51.png)

申请成功<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-52.png)

## CA 服务器颁布客户端证书

器颁布客户端证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-53.png)

## VPN 客户端安装导入客户端证书

### 查看挂起的证书并安装

查看挂起的证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-54.png)

选择客户端证书并安装<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-55.png)

安装成功<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-56.png)

### 下载证书并导入

下载证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-57.png)

下载客户端证书和证书链<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-58.png)

将证书和证书链导入受信任的证书颁发机构(计算机)中<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-59.png)

证书导入成功显示<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-60.png)

证书链导入成功显示<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-61.png)

# VPN 客户端使用 L2TP 方式连接 VPN

连接 VPN<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-62.png)

此时的 VPN 连接状态为 L2TP<br>

# 

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-631.png)

--------------------------------------------------------------------------------

author: 星火燎原@vxhly title: IPSec VPN 的配置 date: 2017-03-02T10:02:33 categories:

* windows tags:
* Security
* Service

--------------------------------------------------------------------------------

IPSec VPN 指采用 IPSec 协议来实现远程接入的一种 VPN 技术, 本篇文章介绍在虚拟机下搭建 IPSec VPN 的全过程（使用 L2TP 连接方式） <!-- more -->

# 环境需求

> 以下实验机全为虚拟机环境。

注意： 外网 IP 地址段： `20.1.1.0` 内网 IP 地址段： `10.1.1.0` VPN 地址池： `10.1.1.100~10.1.1.200` 

## 虚拟环境

### VPN 客户机

**要求：**

1. Windows 系统为： `Windows XP` 
2. 网络适配器连至 `VMnet2` , IP 地址配置为： `20.1.1.2` 

**作用：**

1. 作为 VPN 客户端连接 VPN 服务器提供的 VPN 服务
2. 作为 CA 客户端, 向 CA 服务器申请 CA 客户端证书

### VPN 服务器

**要求：**

1. Windows 系统为： `Windows 2003` 
2. 网络适配器连至 `VMnet2` , IP 地址配置为： `20.1.1.1` 
3. 网络适配器连至 `VMnet1` , IP 地址配置为： `10.1.1.1` 

**作用：**

1. 作为 VPN 服务器为 VPN 客户端提供 VPN 服务
2. 作为 VPN 服务器向 CA 服务器申请 IPSec 证书

### CA 服务器

**要求：**

1. Windows 系统为： `Windows 2003` 
2. 网络适配器连至 `VMnet1` , IP 地址配置为： `10.1.1.2` 

**作用：**

1. 作为 CA 服务器搭建 CA 服务
2. 作为 CA 服务器向 VPN 客户端和 VPN 服务端颁发相关证书

## 网络连通性

> 需保证 VPN 服务器的 VMnet1 可以 ping 通 CA 服务器的 VMnet1

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-1.png)

> 需保证 VPN 客户端的 VMnet2 可以 ping 通 VPN 服务器的 VMnet2

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-2.png)

> 需保证 VPN 客户端的 VMnet2 不可以 ping 通 CA 服务器的 VMnet1

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-3.png)

# VPN 服务器搭建 VPN 服务

## 创建 VPN 用户

创建 VPN 用户： user, 设置其远程访问权限（拨入或 VPN）为通过远程访问策略控制访问<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-4.png)

创建组： vpn, 将用户 user 加入 vpn 组<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-5.png)

## 开启 VPN 服务

### 开启路由和远程访问

开启路由和远程访问<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-6.png)

启用远程访问（拨号或 VPN）<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-7.png)

选择远程访问方式为 VPN<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-8.png)

### 选择连接外网接口

选择连接外网接口<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-9.png)

### 配置 VPN 地址池

配置 VPN 地址池<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-10.png)

VPN 地址池配置为： `10.1.1.100~10.1.1.200` <br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-111.png)

### 配置身份验证方式并完成配置

配置身份验证方式<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-12.png)

完成配置<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-13.png)

### 新建远程访问策略

新建远程访问策略<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-14.png)

自定义策略<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-15.png)

### 添加策略并保存

允许特殊时间段内才能访问<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-16.png)

允许规定用户组才能访问<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-17.png)

授予远程访问权限<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-18.png)

完成向导<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-19.png)

### 网络连通性

> 此时 VPN 客户端的 VMnet2 不可以 ping 通 VPN 服务器的 VMnet2 和 CA 服务器的 VMnet1

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-20.png)

# VPN 客户端连接 VPN 服务器

## 新建一个连接

新建一个连接<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-21.png)

设置连接类型<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-22.png)

使用 VPN 连接<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-23.png)

## 配置连接的 IP 地址

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-24.png)

## 使用 VPN 账户连接 VPN 服务器

连接 VPN 服务器<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-25.png)

此时的 VPN 连接状态为 PPTP<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-26.png)

此时的 IP 地址配置情况<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-27.png)

## 网络连通性

> 此时 VPN 客户端的 VMnet2 可以 ping 通 VPN 服务器的 VMnet1 和 CA 服务器的 VMnet1

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-28.png)

> 此时 VPN 客户端的 VMnet2 不可以 ping 通 VPN 服务器的 VMnet2

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-29.png)

# CA 服务器搭建 CA 服务

## 开启 IIS 服务

开启 IIS 服务<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-30.png)

开启 ASP 配置<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-31.png)

完成配置<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-32.png)

## 安装 CA 证书服务

安装 CA 证书服务<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-33.png)

CA 类型为独立根 CA<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-34.png)

完成安装, 本地访问测试<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-35.png)

# VPN 服务器向 CA 服务器申请 IPSec 证书

申请 IPSec 证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-36.png)

提交一个高级证书申请<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-37.png)

申请 IPSec 证书, 并将证书保存在本地计算机储存中<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-38.png)

申请成功<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-39.png)

## CA 服务器颁布 IPSec 证书

颁布 IPSec 证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-40.png)

## VPN 服务器安装导入 IPSec 证书

### 查看挂起的证书并安装

查看挂起的证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-41.png)

选择 IPSec 证书并安装<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-42.png)

安装成功<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-43.png)

### 下载证书并导入

下载证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-44.png)

下载 IPSec 证书和证书链<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-45.png)

将证书和证书链导入受信任的证书颁发机构(计算机)中<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-46.png)

证书导入成功显示<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-47.png)

证书链导入成功显示<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-48.png)

# VPN 客户机向 CA 服务器申请客户端证书

申请客户端证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-49.png)

提交一个高级证书申请<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-50.png)

申请 IPSec 证书, 并将证书保存在本地计算机储存中<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-51.png)

申请成功<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-52.png)

## CA 服务器颁布客户端证书

器颁布客户端证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-53.png)

## VPN 客户端安装导入客户端证书

### 查看挂起的证书并安装

查看挂起的证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-54.png)

选择客户端证书并安装<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-55.png)

安装成功<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-56.png)

### 下载证书并导入

下载证书<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-57.png)

下载客户端证书和证书链<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-58.png)

将证书和证书链导入受信任的证书颁发机构(计算机)中<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-59.png)

证书导入成功显示<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-60.png)

证书链导入成功显示<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-61.png)

# VPN 客户端使用 L2TP 方式连接 VPN

连接 VPN<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-62.png)

此时的 VPN 连接状态为 L2TP<br>

![IPSec VPN](http://oss-blog.test.upcdn.net/ipsec-vpn-631.png)

