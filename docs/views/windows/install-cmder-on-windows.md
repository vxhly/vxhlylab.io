---
author: 星火燎原@vxhly
title: 在 windows 下安装 Cmder
categories: [windows]
tags: [Liunx, Command]
date: 2017-04-08 14:15:00
---

::: tip
Cmder 是一款 windows 环境下非常简洁美观易用轻量的 cmd 替代者, 它支持了大部分的 Liunx 命令
:::
<!-- more -->

# Cmder 介绍

[Cmder 官网](//cmder.net/)（它把 conemu, msysgit 和 clink 打包在一起, 让你无需配置就能使用一个真正干净的 Linux 终端！它甚至还附带了漂亮的 monokai 配色主题。）; 作为一个压缩档的存在, 可即压即用。你甚至可以放到 USB 就可以虽时带着走, 连调整过的设定都会放在这个目录下, 不会用到系统机码(Registry), 所以也很适合放在 Dropbox / Google Drive / OneDrive 共享于多台电脑。

# 安装 Cmder

推荐去官网下载：[Cmder 官网](//cmder.net/)

下载的时候, 有两个版本, 分别是 mini 与 full 版；唯一的差别在于有没有内建 msysgit 工具, 这是 Git for Windows 的标准配备；全安装版 Cmder 自带了 msysgit, 除了 git 本身这个命令之外, 里面可以使用大量的 linux 命令；比如 grep, curl(没有 wget); 像 vim, grep, tar, unzip, ssh, ls, bash, perl 等。

可能需要翻墙, 这里提供另外的下载地址

> [cmder.zip](http://oss-blog.test.upcdn.net/cmder.zip?attname=) [cmder_mini.zip](http://oss-blog.test.upcdn.net/cmder_mini.zip?attname=)

可能提 `api-ms-crt-runtime` 

> 安装一些运行库 [MSVBCRT_AIO.7z](http://oss-blog.test.upcdn.net/MSVBCRT_AIO.7z?attname=)

# Cmder 元件组成

Cmder 其实结合了多套软体, 其中包括 msysgit 与最重要的 ConEmu 与 Clink 软体, 而 ConEmu 与 Clink 这两套软体就是 Cmder 真正的核心元件。

* **msysgit** 除了提供 `Git for Windows` 相关工具外, 其实还提供了多套 Unix/Linux 环境下常用的指令列工具, 例如 less, ls, tar, unzip, md5sum, grep, sed, ... 等多套工具。光是一个 grep 就不知道比 Windows 内建的 findstr 强几百倍了！
* **ConEmu** 也可以是单独一款软件存在, 曾经一度迷恋于它, 然而其体验并不如 cmder, 便放弃它了。
* **Clink** 将 GNU Readline 函式库整合进原生的 Windows 命令提示字元视窗, 提供命令列模式下强大的编辑与输入能力, 这也是用了 Cmder 之后会这么像在 Linux 环境下使用的感觉。

# 配置 Cmder

## 启动 Cmder

因为它是即压即用的存在, 所以点击 `Cmder.exe` 即可运行。很显然这般打开它, 不怎么快捷, 即便用 Listary 高效搜索到它, 然后点击也是很麻烦的; 

我们可以这样做:

* 把 Cmder 加到环境变量

-- 可以把 `Cmder.exe` 存放的目录添加到系统环境变量；加完之后, `Win+r` 一下输入 cmder, 即可

* 添加 Cmder 到右键菜单

-- 在某个文件夹中打开终端, 这个是一个(超级)痛点需求, 实际上上一步的把 Cmder 加到环境变量就是为此服务的, 在管理员权限的终端输入以下语句即可: `Cmder.exe /REGISTER ALL` 

## 打开一个管理员权限终端

在任意一个 Cmder 窗口输入 `Ctrl+t` , 或者在 Cmder 窗口中点击下方控制条的绿色加号, 勾选 `Run as administrator` 

![安装 Cmder](http://oss-blog.test.upcdn.net/install-cmder-1.png)

## 设置命令的别名

``` bash
la=ls -aF --show-control-chars --color
ll=ls -alF --show-control-chars --color
ls=ls --show-control-chars -F --color
```

添加至 `cmder/config/user-aliases.cmd` 文件末尾

## 修改命令提示符号

Cmder 预设的命列列提示符号是 `λ` ; 如果用着不习惯, 可以将这个字元改成 `Mac/Linux` 环境下常见的 `---
author: 星火燎原@vxhly
title: 在 windows 下安装 Cmder
categories:

-- windows

tags:

-- Liunx
-- Command

abbrlink: 447494ff
date: 2017-04-08T14:15:00
---

Cmder 是一款 windows 环境下非常简洁美观易用轻量的 cmd 替代者, 它支持了大部分的 Liunx 命令 <!-- more -->

# Cmder 介绍

[Cmder 官网](//cmder.net/)（它把 conemu, msysgit 和 clink 打包在一起, 让你无需配置就能使用一个真正干净的 Linux 终端！它甚至还附带了漂亮的 monokai 配色主题。）; 作为一个压缩档的存在, 可即压即用。你甚至可以放到 USB 就可以虽时带着走, 连调整过的设定都会放在这个目录下, 不会用到系统机码(Registry), 所以也很适合放在 Dropbox / Google Drive / OneDrive 共享于多台电脑。

# 安装 Cmder

推荐去官网下载：[Cmder 官网](//cmder.net/)

下载的时候, 有两个版本, 分别是 mini 与 full 版；唯一的差别在于有没有内建 msysgit 工具, 这是 Git for Windows 的标准配备；全安装版 Cmder 自带了 msysgit, 除了 git 本身这个命令之外, 里面可以使用大量的 linux 命令；比如 grep, curl(没有 wget); 像 vim, grep, tar, unzip, ssh, ls, bash, perl 等。

可能需要翻墙, 这里提供另外的下载地址

> [cmder.zip](http://oss-blog.test.upcdn.net/cmder.zip?attname=) [cmder_mini.zip](http://oss-blog.test.upcdn.net/cmder_mini.zip?attname=)

可能提 `api-ms-crt-runtime` 

> 安装一些运行库 [MSVBCRT_AIO.7z](http://oss-blog.test.upcdn.net/MSVBCRT_AIO.7z?attname=)

# Cmder 元件组成

Cmder 其实结合了多套软体, 其中包括 msysgit 与最重要的 ConEmu 与 Clink 软体, 而 ConEmu 与 Clink 这两套软体就是 Cmder 真正的核心元件。

* **msysgit** 除了提供 `Git for Windows` 相关工具外, 其实还提供了多套 Unix/Linux 环境下常用的指令列工具, 例如 less, ls, tar, unzip, md5sum, grep, sed, ... 等多套工具。光是一个 grep 就不知道比 Windows 内建的 findstr 强几百倍了！
* **ConEmu** 也可以是单独一款软件存在, 曾经一度迷恋于它, 然而其体验并不如 cmder, 便放弃它了。
* **Clink** 将 GNU Readline 函式库整合进原生的 Windows 命令提示字元视窗, 提供命令列模式下强大的编辑与输入能力, 这也是用了 Cmder 之后会这么像在 Linux 环境下使用的感觉。

# 配置 Cmder

## 启动 Cmder

因为它是即压即用的存在, 所以点击 `Cmder.exe` 即可运行。很显然这般打开它, 不怎么快捷, 即便用 Listary 高效搜索到它, 然后点击也是很麻烦的; 

我们可以这样做:

* 把 Cmder 加到环境变量

-- 可以把 `Cmder.exe` 存放的目录添加到系统环境变量；加完之后, `Win+r` 一下输入 cmder, 即可

* 添加 Cmder 到右键菜单

-- 在某个文件夹中打开终端, 这个是一个(超级)痛点需求, 实际上上一步的把 Cmder 加到环境变量就是为此服务的, 在管理员权限的终端输入以下语句即可: `Cmder.exe /REGISTER ALL` 

## 打开一个管理员权限终端

在任意一个 Cmder 窗口输入 `Ctrl+t` , 或者在 Cmder 窗口中点击下方控制条的绿色加号, 勾选 `Run as administrator` 

![安装 Cmder](http://oss-blog.test.upcdn.net/install-cmder-1.png)

## 设置命令的别名

``` bash
la=ls -aF --show-control-chars --color
ll=ls -alF --show-control-chars --color
ls=ls --show-control-chars -F --color
```

添加至 `cmder/config/user-aliases.cmd` 文件末尾

## 修改命令提示符号

Cmder 预设的命列列提示符号是 `λ` ; 如果用着不习惯, 可以将这个字元改成 `Mac/Linux` 环境下常见的 符号, 具体操作如下：

编辑 Cmder 安装目录下的 `vendor\clink.lua` 批处理文件(min 版本 15 行, full 版本 41 行), 把：

``` 
local cmder_prompt = "\x1b[1;32;40m{cwd} {git}{hg} \n\x1b[1;30;40m{lamb} \x1b[0m"
```

修改成

``` 
local cmder_prompt = "\x1b[1;32;40m{cwd} {git}{hg} \n\x1b[1;30;40m$ \x1b[0m"
```

其实就是将 `{lamb}` 替换成 `---
author: 星火燎原@vxhly
title: 在 windows 下安装 Cmder
categories:

-- windows

tags:

-- Liunx
-- Command

abbrlink: 447494ff
date: 2017-04-08T14:15:00
---

Cmder 是一款 windows 环境下非常简洁美观易用轻量的 cmd 替代者, 它支持了大部分的 Liunx 命令 <!-- more -->

# Cmder 介绍

[Cmder 官网](//cmder.net/)（它把 conemu, msysgit 和 clink 打包在一起, 让你无需配置就能使用一个真正干净的 Linux 终端！它甚至还附带了漂亮的 monokai 配色主题。）; 作为一个压缩档的存在, 可即压即用。你甚至可以放到 USB 就可以虽时带着走, 连调整过的设定都会放在这个目录下, 不会用到系统机码(Registry), 所以也很适合放在 Dropbox / Google Drive / OneDrive 共享于多台电脑。

# 安装 Cmder

推荐去官网下载：[Cmder 官网](//cmder.net/)

下载的时候, 有两个版本, 分别是 mini 与 full 版；唯一的差别在于有没有内建 msysgit 工具, 这是 Git for Windows 的标准配备；全安装版 Cmder 自带了 msysgit, 除了 git 本身这个命令之外, 里面可以使用大量的 linux 命令；比如 grep, curl(没有 wget); 像 vim, grep, tar, unzip, ssh, ls, bash, perl 等。

可能需要翻墙, 这里提供另外的下载地址

> [cmder.zip](http://oss-blog.test.upcdn.net/cmder.zip?attname=) [cmder_mini.zip](http://oss-blog.test.upcdn.net/cmder_mini.zip?attname=)

可能提 `api-ms-crt-runtime` 

> 安装一些运行库 [MSVBCRT_AIO.7z](http://oss-blog.test.upcdn.net/MSVBCRT_AIO.7z?attname=)

# Cmder 元件组成

Cmder 其实结合了多套软体, 其中包括 msysgit 与最重要的 ConEmu 与 Clink 软体, 而 ConEmu 与 Clink 这两套软体就是 Cmder 真正的核心元件。

* **msysgit** 除了提供 `Git for Windows` 相关工具外, 其实还提供了多套 Unix/Linux 环境下常用的指令列工具, 例如 less, ls, tar, unzip, md5sum, grep, sed, ... 等多套工具。光是一个 grep 就不知道比 Windows 内建的 findstr 强几百倍了！
* **ConEmu** 也可以是单独一款软件存在, 曾经一度迷恋于它, 然而其体验并不如 cmder, 便放弃它了。
* **Clink** 将 GNU Readline 函式库整合进原生的 Windows 命令提示字元视窗, 提供命令列模式下强大的编辑与输入能力, 这也是用了 Cmder 之后会这么像在 Linux 环境下使用的感觉。

# 配置 Cmder

## 启动 Cmder

因为它是即压即用的存在, 所以点击 `Cmder.exe` 即可运行。很显然这般打开它, 不怎么快捷, 即便用 Listary 高效搜索到它, 然后点击也是很麻烦的; 

我们可以这样做:

* 把 Cmder 加到环境变量

-- 可以把 `Cmder.exe` 存放的目录添加到系统环境变量；加完之后, `Win+r` 一下输入 cmder, 即可

* 添加 Cmder 到右键菜单

-- 在某个文件夹中打开终端, 这个是一个(超级)痛点需求, 实际上上一步的把 Cmder 加到环境变量就是为此服务的, 在管理员权限的终端输入以下语句即可: `Cmder.exe /REGISTER ALL` 

## 打开一个管理员权限终端

在任意一个 Cmder 窗口输入 `Ctrl+t` , 或者在 Cmder 窗口中点击下方控制条的绿色加号, 勾选 `Run as administrator` 

![安装 Cmder](http://oss-blog.test.upcdn.net/install-cmder-1.png)

## 设置命令的别名

``` bash
la=ls -aF --show-control-chars --color
ll=ls -alF --show-control-chars --color
ls=ls --show-control-chars -F --color
```

添加至 `cmder/config/user-aliases.cmd` 文件末尾

## 修改命令提示符号

Cmder 预设的命列列提示符号是 `λ` ; 如果用着不习惯, 可以将这个字元改成 `Mac/Linux` 环境下常见的$mdFormatter$16$mdFormatter$符号, 具体操作如下：

编辑 Cmder 安装目录下的 `vendor\clink.lua` 批处理文件(min 版本 15 行, full 版本 41 行), 把：

``` 
local cmder_prompt = "\x1b[1;32;40m{cwd} {git}{hg} \n\x1b[1;30;40m{lamb} \x1b[0m"
```

修改成

``` 
local cmder_prompt = "\x1b[1;32;40m{cwd} {git}{hg} \n\x1b[1;30;40m$ \x1b[0m"
```

其实就是将 `{lamb}` 替换成 就好

# Chocolatey 软件包管理系统

在 Linux 下, 大家喜欢用 `apt-get` (mac 下用 `brew` ) 来安装应用程序, 如今在 windows 下, 大家可以使用 Chocolatey 来快速下载搭建一个开发环境。Chocolatey 的哲学就是完全用命令行来安装应用程序, 它更像一个包管理工具（背后使用 Nuget ）

另外需要说明的是, Chocolatey 只是把官方下载路径封装到了 Chocolatey 中, 所以下载源都是其官方路径, 所以下载的一定是合法的, 但是如果原软件是需要 Licence 注册的话, 那么 Chocolatey 下载安装好的软件还是需要你去购买注册。不过 Chocolatey 一般还是会选用免费 Licence 可用的软件。

## 安装 chocolatey

运行如下命令即可(需要管理员权限)：

``` bash
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin
```

出现 `Chocolatey (choco.exe) is now ready.` 字样表示安装成功

## choco 安装软件

安装软件命令 `choco install softwareName` , 短写是 `cinst softwareName` 

可安装的应用程序, 默认安装在 `C:\Program Files\` , 可以参见其 [Package 列表](//chocolatey.org/packages)

以下是 window 下开发常用的开发环境应用:

``` bash
choco install autohotkey.portable    #安装 AutoHotkey (Portable)
choco install nodejs.install  #安装 node
choco install git.install     #安装 git
choco install ruby            #安装 ruby
choco install python          #安装 python
choco install jdk8            #安装 JDK8
choco install googlechrome    #安装 Chrome
choco install google-chrome-x64 #Google Chrome (64-bit only)
choco install firefox         #安装 firefox
choco install notepadplusplus.install #安装 notepad++
choco install Atom                    #安装 Atom
choco install SublimeText3            #安装 SublimeText3
```

# 快捷键

可以利用 Tab, 自动路径补全 打开设置面板 => `Win+Alt+P` 打开新的页签 => `Ctrl+T` 关闭页签 => `Ctrl+W` 切换页签 => `Ctrl+Tab` 关闭所有页签 => `Alt+F4` 快速打开一个 CMD => `Shift+Alt+1` 快速打开一个 PowerShell => `Shift+Alt+2` 快速打开一个 PowerShell(系统管理员权限) => `Shift+Alt+2` 快速切换到第 1 个页签 => `Ctrl+1` 快速切换到第 n 个页签(n 值无上限) => `Ctrl+n` 历史命令搜索 => `Ctr+r` 全屏 => `Alt+Enter` 

# 另一个重量级工具

> [cygwin](//www.cygwin.com/) 官网<br>
> 此工具非常庞大, 其实就是将 Liunx 上的命令转换成 windows 上可执行的 exe 程序, 同时还会在真机上装一个 Liunx 系统, 占用磁盘大小就在于你装了几个命令, 虽然不推介使用, 但是喜欢折腾的童鞋可以去官网上将其下载下来。

