---
author: 星火燎原@vxhly
title: Vue.js 利用 Express 结合 MySQL 或者 MongoDB
categories: [vuejs]
tags: [Vue.js, Node.js, MySQL, MongoDB, Express]
date: 2017-09-02 10:04:08
---

::: tip
使用 vue-cli 脚手架工具创建一个基于 webpack 的 Vue 项目本身已经包含了 Express, 但是项目若需要结合 MySQL 或者 MongoDB, 还需折腾一番
:::
<!-- more -->

# 构建 Vue 项目

> 前提： 安装 **node.js** 环境

## 安装 vue-cli

``` bash
npm install -g vue-cli
```

## 一个基于 webpack 的 Vue 项目

``` bash
vue init webpack [project-name]
cd [project-name]
npm install
```

## 使用 vue-resource 请求数据

``` bash
npm install --save vue-resource
```

在你的 `src/main.js` 适当位置添加以下代码

``` javascript
import VueResource from "vue-resource"
Vue.use(VueResource)
```

# MySQL 

<!-- > 示例： <https://github.com/vxhly/vue-express-mysql> -->

 

## 添加 Express 服务端目录

> 前提： 安装 **MySQL** 数据库

在项目根文件夹下创建一个 server 文件夹。 然后里面创建下面几个文件夹及文件

``` bash
|-- server
  |-- bin
    |-- www.js      => Express 服务器入口文件
  |-- config
    |-- db.js       => MySQL 数据库配置文件
  |-- map
    |-- sqlMap.js   => SQL 语句存储文件
  |-- router
    |-- userApi.js  => 数据库操作文件
```

### bin/www.js

``` javascript
const express = require("express")
const path = require("path")
const bodyParser = require("body-parser")
const cookieParser = require("cookie-parser")
const logger = require("morgan")
const compression = require("compression")
const app = express()
const userApi = require("../router/userApi")
app.use(logger("dev"))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(cookieParser())
app.use(compression({
    threshold: 0
}))
// 后端api路由
app.use("/api/user", userApi)
// 监听端口
app.use((req, res, next) => {
    var err = new Error("This page not found")
    err.status = 404
    next(err)
})
app.listen(3000, () => {
    console.log("Server running in port 3000...")
})
```

### config/db.js

``` javascript
// 数据库连接配置
module.exports = {
    mysql: {
        host: "127.0.0.1",
        user: "root",
        password: "root",
        database: "test",
        port: "3306"
    }
}
```

### map/sqlMap.js

```javascript
// sql语句
const sqlMap = {

    // 用户
    user: {
        add: "insert into users( `id` , `user_id` , `user_pwd` ) values (NULL, ?, ?)"
    }

}
module.exports = sqlMap
```

### router/userApi.js

``` javascript
const models = require("../config/db")
const express = require("express")
const router = express.Router()
const mysql = require("mysql")
const $sql = require("../map/sqlMap")
// 连接数据库
const conn = mysql.createConnection(models.mysql)
conn.connect()
const jsonWrite = (res, ret) => {
    if (typeof ret === "undefined") {
        res.json({
            code: "1",
            msg: "操作失败"
        })
    } else {
        res.json(ret)
    }
}
// 增加用户接口
router.post("/addUser", (req, res) => {
    var sql = $sql.user.add
    var params = req.body
    console.log(params)
    conn.query(sql, [params.user_id, params.user_pwd], (err, result) => {
        if (err) {
            console.log(err)
        }
        if (result) {
            jsonWrite(res, result)
        }
    })
})
module.exports = router
```

## 添加 scripts 到 package.json

``` json
"scripts": {
  "server": "nodemon ./server/bin/www.js", 
}
```

> 注意到 package.json 中的 nodemon, 这是热加载插件, 就是有新文件的创建或者有新增代码, 将会重新启动 Express 服务

## 安装依赖

``` bash
npm install --save nodemon mysql body-parser
```

# MongoDB

> 示例： <https://github.com/vxhly/vue-express-mongodb>

## 添加 Express 服务端目录

> 前提： 安装 **MongoDB** 数据库

在项目根文件夹下创建一个 server 文件夹。 然后里面创建下面几个文件夹及文件

``` bash
|-- server
  |-- bin
    |-- www.js      => Express 服务器入口文件
  |-- config
    |-- db.js       => MongoDB 数据库配置文件
  |-- router
    |-- userApi.js  => 数据库操作文件
```

### bin/www.js

``` javascript
const express = require("express")
const path = require("path")
const bodyParser = require("body-parser")
const cookieParser = require("cookie-parser")
const logger = require("morgan")
const compression = require("compression")
const app = express()
const userApi = require("../router/userApi")
app.use(logger("dev"))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(cookieParser())
app.use(compression({
    threshold: 0
}))
// 后端api路由
app.use("/api/user", userApi)
// 监听端口
app.use((req, res, next) => {
    var err = new Error("This page not found")
    err.status = 404
    next(err)
})
app.listen(3000, () => {
    console.log("Server running in port 3000...")
})
```

### config/db.js

``` javascript
const mongoose = require("mongoose")
mongoose.connect("mongodb://127.0.0.1/test")
const db = mongoose.connection
mongoose.Promise = global.Promise
db.on("error", console.error.bind(console, "Connect error"))
db.once("open", function() {
    console.log("Mongodb started successfully")
})
const userSchema = mongoose.Schema({
    userId: {
        type: Number,
        required: true
    },
    userPwd: {
        type: String,
        required: true
    }
})
const Models = {
    User: mongoose.model("User", userSchema)
}
module.exports = Models
```

### router/userApi.js

``` javascript
const express = require("express")
const router = express.Router()
const model = require("../config/db.js")
const jsonWrite = (res, ret) => {
    if (typeof ret === "undefined") {
        res.json({
            code: "404",
            msg: "server is error"
        })
    } else {
        res.json(ret)
    }
}
router.post("/useradd", (req, res, next) => {
    const params = req.body
    const id = params.userId
    const pwd = params.userPwd
    const user = new model.User({
        userId: id,
        userPwd: pwd
    })
    model.User.findOne({
            userId: user.userId
        },
        (err, doc) => {
            if (err) console.log(err)
            console.log(doc)
            if (doc) {
                jsonWrite(res, doc)
            } else {
                user.save(err => {
                    if (err) {
                        console.log(err)
                    } else {
                        jsonWrite(res, doc)
                    }
                })
            }
        })
})
module.exports = router
```

# 编写 vue 测试文件

``` html
<template>
    <div class="hello">
        <h1>{{ msg }}</h1>
        <form>
            <input type="text" name="user_id" v-model="user_id" placeholder="Enter your ID"> <br>
            <input type="password" name="user_pwd" v-model="user_id" placeholder="Enter your password"> <br>
            <a href="javascript:" @click="addUser">提交</a>
        </form>
    </div>
</template>

<script>
    export default {
        name: 'hello',
        data() {
            return {
                msg: 'Welcome to Your Vue.js App',
                user_id: '',
                user_pwd: ''
            }
        },
        methods: {
            addUser() {
                var userId = this.user_id
                var userPwd = this.user_pwd
                this.$http.post('/api/user/addUser', {
                    user_id: userId,
                    user_pwd: userPwd
                }).then((response) => {
                    console.log(response)
                })
            }
        }
    }
</script>
```

# 设置代理与跨域

完成上面步骤之后, 执行 `npm run dev` , 你会发现会报一个错误： `vue-resource.common.js?e289:1071 POST http://localhost:8080/api/use... 404 (Not Found)` 。 这是由于直接访问 8080 端口, 是访问不到的, 所以这里需要设置一下代理转发映射.

项目根目录下的 config 文件夹中有一个 proxyTable 参数, 用来设置地址映射表, 可以添加到开发时配置（dev）中

## config/index.js

``` javascript
dev: {
    // ...
    proxyTable: {
        '/api': {
            target: 'http://127.0.0.1:3000/api/',
            changeOrigin: true,
            pathRewrite: {
                '^/api': ''
            }
        }
    },
    // ...
}
```

添加以上代码之后, 请求 `/api` 时就代表 `http://127.0.0.1:3000/api/` (这里要写 ip, 不要写 localhost), changeOrigin 参数接收一个布尔值, 如果为 true, 这样就不会有跨域问题了。

# 开启服务

``` bash
npm run dev
npm run server
```

