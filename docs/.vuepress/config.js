module.exports = {
  'title': 'Coding and Fixing',
  'description': '源于生活而高于生活',
  'dest': 'public',
  'head': [
    [ 'link', { 'rel': 'icon', 'href': '/favicon.ico' } ],
    [ 'link', { 'rel': 'manifest', 'href': '/manifest.json' } ],
    [ 'meta', { 'name': 'theme-color', 'content': '#22979b' } ],
    [ 'meta', { 'name': 'apple-mobile-web-app-capable', 'content': 'yes' } ],
    [ 'meta', { 'name': 'apple-mobile-web-app-status-bar-style', 'content': '#22979b' } ],
    [ 'link', { 'rel': 'apple-touch-icon', 'href': '/icons/apple-touch-icon-152x152.png' }],
    [ 'link', { 'rel': 'mask-icon', 'href': '/icons/safari-pinned-tab.svg', 'color': '#2c2c2c' }],
    [ 'meta', { 'name': 'msapplication-TileImage', 'content': '/icons/msapplication-icon-144x144.png' } ],
    [ 'meta', { 'name': 'msapplication-TileColor', 'content': '#000000' } ],
    [ 'meta', { 'name': 'viewport', 'content': 'width=device-width,initial-scale=1,user-scalable=no' } ]
  ],
  'theme': 'reco',
  'themeConfig': {
    'themePicker': false,
    'nav': [
      {
        'text': 'Home',
        'link': '/',
        'icon': 'reco-home'
      },
      {
        'text': 'TimeLine',
        'link': '/timeLine/',
        'icon': 'reco-date'
      },
      {
        'text': 'Contact',
        'icon': 'reco-message',
        'items': [
          {
            'text': 'NPM',
            'link': 'https://www.npmjs.com/~vxhly',
            'icon': 'reco-npm'
          },
          {
            'text': 'GitHub',
            'link': 'https://github.com/vxhly',
            'icon': 'reco-github'
          }
          // {
          //   'text': '简书',
          //   'link': 'https://www.jianshu.com/u/cd674a19515e',
          //   'icon': 'reco-email'
          // },
          // {
          //   'text': '博客圆',
          //   'link': 'https://www.cnblogs.com/luanhewei/',
          //   'icon': 'reco-bokeyuan'
          // },
          // {
          //   'text': 'WeChat',
          //   'link': 'https://mp.weixin.qq.com/s/mXFqeUTegdvPliXknAAG_A',
          //   'icon': 'reco-wechat'
          // }
        ]
      }
    ],
    'type': 'blog',
    'blogConfig': {
      'category': {
        'location': 2,
        'text': 'Category'
      },
      'tag': {
        'location': 3,
        'text': 'Tag'
      }
    },
    'logo': '/favicon.ico',
    'search': true,
    'searchMaxSuggestions': 10,
    'sidebar': 'auto',
    'lastUpdated': 'Last Updated',
    'author': '星火燎原@vxhly',
    'record': '',
    'startYear': '2016'
  },
  'markdown': {
    'lineNumbers': true
  },
  'plugins': [
    ['@vuepress/medium-zoom',{
      'selector': '.page img',
      'options': {
        'margin': 16,
        'background': '#202124de',
        'scrollOffset': 0
      }
    }],
    ['@vuepress/pwa', {
      'serviceWorker': true,
      'updatePopup': true
    }],
    'flowchart',
    require('./plugins/clipboard-copy/clipboard-copy'),
    ['copyright', {
      'noCopy': true, 
      'minLength': 150,
      // the selected text will be uncopiable
      // disable the plugin by default
      // you can activate the plugin in frontmatter
      'disabled': false,
      // // texts will be unselectable
      'noSelect': true
    }]
  ]
}